import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  Animated,
  Easing,
  StatusBar,
  ImageBackground,
} from 'react-native';
import Drawer from 'react-native-drawer'
 
import { Button, Input } from 'react-native-elements';
export default class HomePage extends React.Component<{}> {
	static navigationOptions = { header: null }
      render() {
        var {navigate} = this.props.navigation;
        return (
          <View style={{flex: 1,justifyContent: 'center', flexDirection:'column'}}>
            <StatusBar  
              backgroundColor="#b2dd4c"
              barStyle="light-content"
            />
            <Image
              style = {styles.images}
              source={require('./assets/images/logodark.png')} 
            />
            <Text
            style={styles.subtitle}> C'est si simple de vendre mieux </Text>

          </View>
        );
      }
    }    

  
  const styles = StyleSheet.create({
  images: {
    width: 330,
    height:55,
    alignSelf:'center',
  },
  subtitle : {
    fontSize:20,
    justifyContent:'center',
    alignSelf:'center',
    marginTop:20,
  },
  buttonStyleInactive: {
    borderRadius: 5,
    backgroundColor: 'rgb(247,209,7)',
    borderColor:"rgb(247,209,7)",
    borderWidth: 1,
    height: 40,
    marginRight:20,
    marginTop: 170,
    elevation:0,
    width: 130,
  },

  imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1 
},

  buttonStyleActive: {
    borderRadius: 5,
    backgroundColor: '#4a4a4a',
    borderColor:"#4a4a4a",
    borderWidth: 1,
    height: 40,
    marginTop: 170,
    marginRight:10,
    elevation:0,
    width: 130,
  },

  titleMain: {
      marginTop:30,
      fontSize:16,
      alignSelf:'center',
      color:"white",
  }
})
