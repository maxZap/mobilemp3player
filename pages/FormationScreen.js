import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
    Dimensions,
  Animated,
  Easing,
} from 'react-native';
import { Button, Input, } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as rssParser from 'react-native-rss-parser';


export default class FormationScreen extends React.Component<{}> {

  static navigationOptions = { header: null }
      render() {
        return (
          <View style={{flex: 1,
        	flexDirection: 'column',
        	justifyContent: 'center',
        	alignItems: 'center',}}>  
            <Text> FormationScreen </Text>
          </View>
        );
      }
    }    



const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 20,
    height: 30,
    borderBottomColor: 'rgb(247,209,7)',
    borderBottomWidth: 1,
  },

  buttonStyle: {
    borderRadius: 5,
    backgroundColor: 'rgb(247,209,7)',
    borderColor:"rgb(247,209,7)",
    borderWidth: 1,
    height: 40,
    marginTop: 20,
    marginRight:10,
    elevation:0,
    width: 130,
  },
})
