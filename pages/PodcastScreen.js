import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Animated,
  Easing,
  ScrollView
} from 'react-native';
import showPodcast from './showPodcast.js'
import {createBottomTabNavigator,TabNavigator, createStackNavigator} from 'react-navigation';
//https://www.spreaker.com/show/2700880/episodes/feed
import { Button, Input, ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as rssParser from 'react-native-rss-parser';
export default class PodcastScreen extends React.Component<{}> {
static navigationOptions = { header: null }
    
   constructor(props) {
      super(props);
      this.state = {
          isLoading:true,
          dataSource:[],
          podcastData:[]
      };
    }

    getPodcast = () => {
    if(this.state.isLoading == true) { 
      return fetch('https://www.spreaker.com/show/2700880/episodes/feed') 
      .then((response) => response.text())
       .then((responseData) => rssParser.parse(responseData))
       .then((rss) => {
          this.setState({ 
              isLoading:false,
              dataSource:rss['items']
            
          })
        var audioFiles = new Array()  
        for (i in this.state.dataSource) {
          audioFiles.push({id:i, mp3: rss["items"][i]['enclosures'][0]['url'], title: rss["items"][i]['title'], description:rss["items"][i]['description'], icon:'./assets/images/arrow.svg'});
        }
        this.setState({ podcastData: audioFiles})
      })
      .catch((error) => {
        console.error(error);
      });
    }
  }

  render() {
    var {navigate} = this.props.navigation;
    this.getPodcast();
    return (
      <ScrollView>
         {
            this.state.podcastData.map((l, i) => (
              <ListItem
                key={i}
                title={l.title}
                rightIcon={{ source: require('./assets/images/arrow.svg') }}
                onPress={() => navigate("showPodcast", {audioId:i, audio:l.mp3, title:l.title, description:l.description})}
              />
            ))
          }
      </ScrollView>
    );
  }
} 

const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 20,
    height: 30,
    borderBottomColor: 'rgb(247,209,7)',
    borderBottomWidth: 1,
  },

  buttonStyle: {
    borderRadius: 5,
    backgroundColor: 'rgb(247,209,7)',
    borderColor:"rgb(247,209,7)",
    borderWidth: 1,
    height: 40,
    marginTop: 20,
    marginRight:10,
    elevation:0,
    width: 130,
  },
})
