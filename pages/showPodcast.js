import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Animated,
  Easing,
  ScrollView,
} from 'react-native';
import SoundPlayer from 'react-native-sound-player'
import { Button, Input, Avatar } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import AudioControls from '../components/audioPlayer/AudioControls';
import { setPodcast, getPodcast } from '../components/StoreData';
export default class showPodcast extends React.Component<{}> {
  static navigationOptions = { header: null }
    
  componentWillMount(){
    const {state} = this.props.navigation;
    var Id = state.params ? state.params.audioId : "<undefined>";
    var title = state.params ? state.params.title : "<undefined>";
    var description = state.params ? state.params.description : "<undefined>";
    var mp3Files = state.params ? state.params.audio : "<undefined>";
    AudioControls();
    setPodcast('UniquePodcast',Id, title, description, mp3Files);
  }


  render() {
    const {state} = this.props.navigation;
    var Id = state.params ? state.params.audioId : "<undefined>";
    var description = state.params ? state.params.description : "<undefined>";
    var mp3Files = state.params ? state.params.audio : "<undefined>";
    var title = state.params ? state.params.title : "<undefined>";
    var {navigate} = this.props.navigation;
    
     
    return (

      <ScrollView style={{flex: 1,}}>
        <Text> {title} </Text>
          <View style={{flex: 1,}}>
        <Text> {description} </Text>
        <Text onPress={() => navigate("readTrackScreen", {id:Id, audio:mp3Files, title:title, description:description})}> Ecouter le mp3  </Text>
      </View>
    </ScrollView>  
    );
  }
}    



const styles = StyleSheet.create({
  inputContainer: {
    marginBottom: 20,
    height: 30,
    borderBottomColor: 'rgb(247,209,7)',
    borderBottomWidth: 1,
  },

  buttonStyle: {
    borderRadius: 5,
    backgroundColor: 'rgb(247,209,7)',
    borderColor:"rgb(247,209,7)",
    borderWidth: 1,
    height: 40,
    marginTop: 20,
    marginRight:10,
    elevation:0,
    width: 130,
  },
})
