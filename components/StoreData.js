import { AsyncStorage } from "react-native";

	 const setPodcast = async(key ,id, title, description, files) => {
    	AsyncStorage.getItem(key)
	    .then(async(favs) => {
	      favs = favs == null ? [] : JSON.parse(favs)
	      favs.push({'id':id, 'title':title, 'description':description, 'files':files})
	      favs = Array.isArray(favs) ? favs : [favs]
	      return await AsyncStorage.setItem(key, JSON.stringify(favs))
	    })
	}

	const getData = async(key) => {
		return await AsyncStorage.getItem(key).then(async(value) => {
            return JSON.parse(value);
        });
	}

	const deletePodcast = async (key) => {
	  try {
	    await AsyncStorage.removeItem(key);
	  } catch (error) {
	    console.log(error.message);
	  }
}


export { setPodcast, getData ,deletePodcast }
	
