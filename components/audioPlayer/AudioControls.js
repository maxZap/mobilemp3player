import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    Slider,
    Text,
    Dimensions,
    ScrollView
} from 'react-native';
import images from './config/images.js';
import colors from './config/color.js';
import AudioController from './AudioController.js';

const { width } = Dimensions.get('window');

class AudioControls extends Component {
    static defaultProps = {
        ...Component.defaultProps,
        //SKIP SECONDS
        hasButtonSkipSeconds: false,
        timeToSkip: 15,

        //THUMBNAIL
        thumbnailSize: {
            width: width * 0.6,
            height: width * 0.6
        },

        //SOUND
        titleStyle: {
            fontSize: 18,
            fontWeight: 'bold',
            marginLeft:10,
            marginRight:10,
            color: colors.white,
            textAlign:'center',
        },
        authorStyle: {
            fontSize: 16,
            color: colors.white
        },

        //COLORS
        activeColor: colors.white,
        inactiveColor: colors.grey,

        //BUTTONS
        activeButtonColor: null,
        inactiveButtonColor: null,

        //SLIDER
        sliderMinimumTrackTintColor: null,
        sliderMaximumTrackTintColor: null,
        sliderThumbTintColor: null,
        sliderTimeStyle: {
            fontSize: 18,
            color: colors.white
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            duration: 0,
            isMounted:false,
            currentTime: 0,
            currentAudio: {},
            isReady: true,
            isPlaying: false
        };
    }

    componentDidMount() {
        const { playlist, initialTrack } = this.props;
        AudioController.init(playlist, initialTrack, this.onChangeStatus, this.updateCurrentTime);
        
    }


    onChangeStatus = (status) => {
        switch (status) {
            case AudioController.status.PLAYING:
                this.setState({ isPlaying: true });
                break;
            case AudioController.status.PAUSED:
                this.setState({ isPlaying: false });
                break;
            case AudioController.status.STOPPED:
                this.setState({ isPlaying: false });
                break;
            case AudioController.status.LOADED:
                AudioController.getDuration((seconds) => {
                    this.setState({ duration: seconds });
                });
                this.setState({ currentAudio: AudioController.currentAudio });
                break;
            case AudioController.status.ERROR:
              
                break;
            default:
                return;
        }
    }

    updateCurrentTime = (seconds) => {
        this.setState({ currentTime: seconds });
    }

    renderPlayerIcon() {
        const { isPlaying } = this.state;
        if (isPlaying) {
            return (
                <TouchableOpacity
                    onPress={() => AudioController.pause()}
                >
                    <Image
                        source={images.iconPause}
                        style={[
                            styles.playButton,
                            { tintColor: this.props.activeButtonColor || this.props.activeColor }
                        ]}
                    />
                </TouchableOpacity >
            );
        }

        return (
            <TouchableOpacity
                onPress={() => AudioController.play()}
            >
                <Image
                    source={images.iconPlay}
                    style={[
                        styles.playButton,
                        { tintColor: this.props.activeButtonColor || this.props.activeColor }
                    ]}
                />
            </TouchableOpacity >
        );
    }

    renderNextIcon() {
        if (AudioController.hasNext()) {
            return (
                <TouchableOpacity onPress={() => AudioController.playNext()}>
                    <Image
                        source={images.iconNext}
                        style={[
                            styles.controlButton,
                            { tintColor: this.props.activeButtonColor || this.props.activeColor }
                        ]}
                    />
                </TouchableOpacity>
            );
        }
        return (
            <Image
                source={images.iconNext}
                style={[
                    styles.controlButton,
                    { tintColor: this.props.inactiveButtonColor || this.props.inactiveColor }
                ]}
            />
        );
    }

    renderPreviousIcon() {
        if (AudioController.hasPrevious()) {
            return (
                <TouchableOpacity onPress={() => AudioController.playPrevious()}>
                    <Image
                        source={images.iconPrevious}
                        style={
                            [styles.controlButton,
                            { tintColor: this.props.activeButtonColor || this.props.activeColor }
                            ]}
                    />
                </TouchableOpacity>
            );
        }
        return (
            <Image
                source={images.iconPrevious}
                style={[
                    styles.controlButton,
                    { tintColor: this.props.inactiveButtonColor || this.props.inactiveColor }
                ]}
            />
        );
    }

    renderSkipbackwardIcon() {
        if (!this.props.hasButtonSkipSeconds) return;
        return (
            <TouchableOpacity
                onPress={() => {
                    AudioController.seekToForward(-this.props.timeToSkip);
                }}
            >
                <Image
                    source={images.skipBackward}
                    style={[
                        styles.controlButton,
                        { tintColor: this.props.activeButtonColor || this.props.activeColor }
                    ]}
                />
            </TouchableOpacity>
        );
    }

    renderSkipforwardIcon() {
        if (!this.props.hasButtonSkipSeconds) return;
        return (
            <TouchableOpacity
                onPress={() => {
                    AudioController.seekToForward(this.props.timeToSkip);
                }}
            >
                <Image
                    source={images.skipForward}
                    style={[
                        styles.controlButton,
                        {
                            tintColor: this.props.activeButtonColor
                                || this.props.activeColor
                        }]}
                />
            </TouchableOpacity>
        );
    }

    render() {
        const { currentTime, duration, currentAudio } = this.state;
        const descriptionSource = currentAudio.descriptionText
        return (
            <View style={styles.container}>
                    <Text style={this.props.titleStyle}>{currentAudio.title}</Text>
                <ScrollView style={styles.detailContainer}>
                <Text style={styles.descriptionText}>
                    {descriptionSource}
                </Text>
                </ScrollView>
                <View style={styles.playbackContainer}>
                    
                    <Slider
                        value={currentTime}
                        maximumValue={duration}

                        style={styles.playbackBar}

                        minimumTrackTintColor={this.props.sliderMinimumTrackTintColor ||
                            this.props.activeColor}
                        maximumTrackTintColor={this.props.sliderMaximumTrackTintColor ||
                            this.props.inactiveColor}
                        thumbTintColor={this.props.sliderThumbTintColor || this.props.activeColor}

                        onSlidingComplete={seconds => {
                            AudioController.seek(seconds);
                            if (seconds < duration) AudioController.play();
                        }}

                        onValueChange={() => AudioController.clearCurrentTimeListener()}
                    />
                   
                </View>
                <View style={styles.buttonsContainer}>
                    {this.renderSkipbackwardIcon()}
                    {this.renderPreviousIcon()}
                    {this.renderPlayerIcon()}
                    {this.renderNextIcon()}
                    {this.renderSkipforwardIcon()}
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    descriptionText: {
        color:'#FFF',
        margin:20,
    },
    detailContainer: {
        marginTop: 15,
        marginVertical: 10
    },
    playbackContainer: {
        flexDirection: 'row'
    },
    buttonsContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    playbackBar: {
        width: '70%'
    },
    playButton: {
        width: 80,
        height: 80
    },
    controlButton: {
        width: 20,
        height: 20,
        margin: 5
    }
});

export default AudioControls;