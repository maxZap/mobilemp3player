const images = {
	iconPlay: require('../../../pages/assets/images/player/play.png'),
	iconPause: require('../../../pages/assets/images/player/pause.png'),
	iconPrevious: require('../../../pages/assets/images/player/left-arrow.png'),
	iconNext: require('../../../pages/assets/images/player/right-arrow.png'),
	skipForward: require('../../../pages/assets/images/player/skip-forward.png'),
	skipBackward: require('../../../pages/assets/images/player/skip-backward.png'),
	thumbnail: require('../../../pages/assets/images/player/thumbnail.jpg'),
};


export default images;

