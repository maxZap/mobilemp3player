/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Header
} from 'react-native';
import { YellowBox } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator,TabNavigator, createStackNavigator} from 'react-navigation';
import PodcastScreen from './pages/PodcastScreen.js'
import showPodcast from './pages/showPodcast.js'
import FormationScreen from './pages/FormationScreen.js'
import HomeScreen from './pages/HomeScreen.js'
import readTrackScreen from './pages/readTrackScreen.js'

export default class App extends React.Component {
  render() {
    return <HiddenStack/>;    
  }
}

const Tabs = createBottomTabNavigator({
    Home: {screen: HomeScreen},
    Podcast: {screen: PodcastScreen},
    Formation: {screen: FormationScreen}, 
    
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
      },
    }),
    tabBarOptions: {
      activeTintColor: "#6200EE",
      inactiveTintColor: "#858585",
      style: {
        height: 40,
        backgroundColor: "#b2dd4c"
      },
      labelStyle: {
        fontSize: 12,
        color:"#212121",
        paddingBottom:10
      }
    },
    animationEnabled: true,
    swipeEnabled: true
  }
); 

const HiddenStack = createStackNavigator({
  showPodcast: {screen:showPodcast},
  readTrackScreen: {screen: readTrackScreen}, 

  Tabs: {
     screen: Tabs
  },
}, 
{
   initialRouteName: "Tabs",
    headerMode: 'none',
    navigationOptions: {
    headerVisible: false,
  }

});

/*const RootStack = createBottomTabNavigator  (
  {

    Home: {screen: HomeScreen},
    Podcast: {screen: PodcastScreen},
    Formation: {screen: FormationScreen}, 
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
      },
    }),
    tabBarOptions: {
      activeTintColor: "#6200EE",
      inactiveTintColor: "#858585",
      style: {
        height: 40,
        backgroundColor: "#b2dd4c"
      },
      labelStyle: {
        fontSize: 12,
        color:"#212121",
        paddingBottom:10
      }
    },
    animationEnabled: true,
    swipeEnabled: true
  }
);*/
